import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TaskServiceService } from 'src/app/services/task-service.service';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.css']
})
export class TaskManagerComponent implements OnInit {

  description: string = "";
  descriptionIntput = new FormControl('');
  taskList: any[];

  constructor(private taskService: TaskServiceService) { }
  

  ngOnInit(): void {
    this.loadAll();
  }

  save = () =>{
    var description = this.descriptionIntput.value;
    this.descriptionIntput.setValue("");
    this.taskService.createTask(description).subscribe(obj => {
      this.loadAll();
    }
    );
  }

  loadAll = () => {
    this.taskService.getAllTask().subscribe(r =>{
      this.taskList = r;
    });
  }

  delete(id: string){
    this.taskService.deleteTask(id).subscribe(response => {
      this.loadAll();
    });
  }

}
