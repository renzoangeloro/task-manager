import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {

  
  constructor(private httpClient: HttpClient) { }

  HOST: String = "http://localhost:8080/task";

  createTask = (description: string): Observable<HttpResponse<any>> => {
    return this.httpClient.post<any>(`${this.HOST}/create`, description );
  }

  deleteTask = (id: string): Observable<HttpResponse<any>> => {
    return this.httpClient.delete<any>(`${this.HOST}/delete/${id}`);
  }

  getAllTask = (): Observable<any[]> => { 
    return this.httpClient.get<any[]>(`${this.HOST}/taskList`);
  }
}
