package com.renzoangeloro.taskmanager.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.renzoangeloro.taskmanager.dtos.TaskResponseDTO;
import com.renzoangeloro.taskmanager.entities.Task;
import com.renzoangeloro.taskmanager.services.TaskService;

@RestController
@RequestMapping("task")
public class TaskResource {
	
	@Autowired
	private TaskService taskService;
	
	@PostMapping("/create")
	public Task createTask(@RequestBody String descriptionTask) {
		return this.taskService.createTask(new Task(descriptionTask));
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteTask(@PathVariable("id") Long id){
		this.taskService.deleteTask(id);
	}
	
	@GetMapping("/taskList")
	public List<TaskResponseDTO> getTaskList() {
		List<TaskResponseDTO> response = this.taskService.getAllTask();
		return response;
	}

}
