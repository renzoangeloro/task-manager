package com.renzoangeloro.taskmanager.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.renzoangeloro.taskmanager.dtos.TaskResponseDTO;
import com.renzoangeloro.taskmanager.entities.Task;
import com.renzoangeloro.taskmanager.repository.TaskRepository;

@Service
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;
	
	public Task createTask(Task task) {
		return this.taskRepository.save(task);
	}
	
	public List<TaskResponseDTO> getAllTask(){
		List<TaskResponseDTO> response = new ArrayList<>();
		for(Task task :this.taskRepository.findAll()) {
			response.add(new TaskResponseDTO(task));
		};
		return response;
	}
	
	public void deleteTask(Long id) {
		Optional<Task> task = this.taskRepository.findById(id);
		if(task.isPresent()) {
			this.taskRepository.delete(task.get());;
		}
	}
}
