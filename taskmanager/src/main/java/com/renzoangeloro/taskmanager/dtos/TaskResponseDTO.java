package com.renzoangeloro.taskmanager.dtos;

import com.renzoangeloro.taskmanager.entities.Task;

public class TaskResponseDTO {
	
	Long id;
	String description;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public TaskResponseDTO() {}
	
	public TaskResponseDTO(Task task) {
		this.id = task.getId();
		this.description = task.getDescription();
	}
	
}
